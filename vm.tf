data "google_compute_image" "debian" {
  family  = "ubuntu-1804-lts"
  project = "gce-uefi-images"
}

# Creates a GCP VM Instance.
resource "google_compute_instance" "vm" {
  name         = var.name
  machine_type = var.machine_type
  zone         = var.zone
  tags         = ["http-server"]
  labels       = var.labels

  boot_disk {
    initialize_params {
      image = data.google_compute_image.debian.self_link
    }
  }

  network_interface {
    #network = "default"
    network = google_compute_network.vpc_network.self_link    
    access_config {
      // Ephemeral IP
    }
  }

  metadata_startup_script = data.template_file.nginx.rendered

}

resource "google_compute_network" "vpc_network" {
  name                    = "webserver-network"
  auto_create_subnetworks = "true"
}

resource "google_compute_firewall" "http-server" {
  name    = "default-allow-http-terraform"
  network = google_compute_network.vpc_network.self_link

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  // Allow traffic from everywhere to instances with an http-server tag
  source_ranges = ["0.0.0.0/0","10.183.0.0/20","10.184.0.0/14","10.188.0.0/20"]
  target_tags   = ["http-server"]
}

data "template_file" "nginx" {
  template = "${file("${path.module}/template/install_nginx.tpl")}"

}
