#!/bin/bash
set -e
apt update
apt install -y nginx
ufw allow 'Nginx HTTP'
systemctl enable nginx
systemctl restart nginx

apt install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
apt-key fingerprint 0EBFCD88
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt-get install -y docker-ce docker-ce-cli containerd.io
systemctl enable docker
systemctl restart docker

echo "*****   Startup script completes!!    *****"
